import {Component} from 'angular2/core';


@Component({
    selector: 'hello-world',
    template: `
        <div class="panel panel-default">
            <div class="panel-heading">Tasks No. 3, 4, 5</div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="nameInput" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="nameInput" [(ngModel)]="yourName" placeholder="Enter a name here">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="patternInput" class="col-sm-2 control-label">Pattern</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="patternInput" [(ngModel)]="patternText"
                            placeholder="Enter a pattern here">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="notificationInput" class="col-sm-2 control-label">Notification</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="notificationInput" [(ngModel)]="notificationText"
                            placeholder="Enter a notification here">
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <h1 [hidden]="!yourName">Hello {{yourName}}!</h1>

        <div [hidden]="yourName != 'Mariusz'">
            <div class="alert alert-info" role="alert" align="middle">
                <h3><strong>Mariusz</strong> is lecturing in <strong>C025</strong>!</h3>
            </div>
        </div>

        <div [hidden]="yourName != patternText || patternText == '' || notificationText == ''">
            <div class="alert alert-success" role="alert" align="middle">
                <h3>{{notificationText}}</h3>
            </div>
        </div>
    `
})

export class HelloWorld {
  // Declaring the variable for binding with initial value
  yourName: string = '';
  patternText: string = '';
  notificationText: string = '';
}
