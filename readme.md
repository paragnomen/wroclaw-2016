# Course details

Course on advanced programming techniques.
Topics covered: 

   * Web Technologies: Angular 2 + NodeJS tools
   * Blockchain: technical introduction


# Lectures

   * [Lecture 1](https://docs.google.com/presentation/d/1Wz_szSyHGyPrDQfMca9_ntRRHkC21jLbiOddQ9Akg28/edit?usp=sharing)
   * [Lecture 2](https://docs.google.com/presentation/d/1zxme1rtnNt5P0rVYyhXKJMVKt6kP7fTy2oBeRRPD89E/edit?usp=sharing)
   * [Lecture 3](https://ipfs.io/#alpha-demo) IPFS alpha demo
   * [Lecture 4](https://www.youtube.com/watch?v=Lx9zgZCMqXE) Bitcoin Basics
   * [Lecture 5](https://www.youtube.com/watch?v=glyQy_e5LmM) Bitcoin Anonymity
   * [Lecture 6](https://www.youtube.com/watch?v=U_LK0t_qaPo) Ethereum Basics
   


# Labs

## Lab 00

   1. Install NodeJS on your laptop/computer. Version 5.10.1
   2. Install 'gulp' globally on your system, by running: *npm install -g gulp*        
   3. Go and check the Hello World tutorial on [https://angular.io](https://angular.io/)  
   4. Try to replicate the code, and get it to run on your laptop.

## Lab 01

   1. Go to lab01 in this repo
   2. Run

```
#!bash

npm install
gulp
gulp serve
```

Notes: it is OK to accept the two WARNINGS about version mismatch - the newer versions have certain things fixed, better to use the newer versions. 


## Lab 02

   * Download and install Ethereum Go client. Launch on a test network.
   * Download and install Ethereum miner. Launch on a test network. Mine some ether.
   * Download and install [Ethereum-Wallet](https://github.com/ethereum/mist/releases)
   * Setup your account, and sync up with the Test network. You can connect to your own Ethereum node if you want.
   * Send some testnet ether from one address to another within your own wallet.
   * Send some testnet ether to your friends.


## Lab 03

   * [Tutorial for crypto-tokens](https://www.ethereum.org/token)


# Homeworks

## Task 1 - passed

In this folder

        QmZMTpjjDBc3RzQsNR4tnh6Wu7iTG5BPB9N1PAnL27VLcp

create a file: *yourname.surname-email.txt*
and put your email inside. Add your newly created file to IPFS. Remember the new hash somewhere.


## Task 2 - passed

   1. Clone the fork of this repo.
   2. Make a pull-request to upstream, with your name added to students.txt file.


## Task 3 - deadline: May 20

   1. Modify Lab01 so that it uses Twitter Bootstrap, or Google's Material Design UI elements.
   2. Make it look *nicer*.
   3. Make a pull request with your changes.


## Task 4 - deadline: May 22

   1. Add to Lab01 another component on top of the page (make this a class in a separate .ts file), that provides a simple notification message to the user. 
   2. Normally, the notification is invisible, but when the field 'yourName' of the HelloWorld component has a value 'Mariusz', make the notification to show up with a message to the user 'Mariusz is lecturing in C025'
   3. Make a pull request with your working solution.


## Task 5 - deadline: May 22

   1. Modify Task 4 in such a way, that user is presented with two additional text input fields: one for the pattern to be detected from 'yourName', and the other for the notification when the pattern is found in 'yourName'. 
   2. Make a pull request with your working solution.