# Task 6 - the hash wallet

Students:
	- Michał Mosion
	- Tomasz Topczewski
	- Adam Włodarczyk


This application can be modified to store the data by the IPFS. 
The main point of such enhancement would be a creation of hash-tracking system. This system can be provied by tracking the blockchain in Ethereum. Since any change in files which are deployed on IPFS changes the hash of whole application, Ethereum blockchains would be a some kind of the newest IPFS hash repository.  
That would allow to store and access hashes (in wallet application) by many users in contrary to current version which stores in client-side.