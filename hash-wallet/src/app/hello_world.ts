import {Component} from 'angular2/core';


@Component({
    selector: 'hello-world',
    template: `
        {{Show()}}
        <div class="panel panel-default">
            <div class="panel-heading">Hash Wallet</div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="hashInput" class="col-sm-2 control-label">Hash</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="hashInput" [(ngModel)]="hashText" placeholder="Enter a hash here">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="nameInput" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="nameInput" [(ngModel)]="nameText"
                            placeholder="Enter a name here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="typeInput" class="col-sm-2 control-label">Type</label>
                        <div class="col-sm-5">
                            <select class="form-control" id="typeInput" [(ngModel)]="typeText">
                            <option>BTC</option>
                            <option>ETH</option>
                            <option>IPFS</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5">
                            <button type="button" class="btn btn-primary" (click)="Add();Show()"
                            [disabled]="!hashText || !nameText || !typeText">
                            Add hash
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="container">
          <h2>Hashes Table</h2>
          <p>The table conatins hash, name, type.</p>
          <table class="table table-bordered" id="hashesTable">
            <thead>
              <tr>
                <th>Hash</th>
                <th>Name</th>
                <th>Type</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    `
})

export class HelloWorld {
    hashText: string = '';
    nameText: string = '';
    typeText: string = '';
    hashesArray: String[];


    Add() {
        if (typeof (Storage) !== 'undefined') {
            if (localStorage.getItem('hashes')) {
                this.hashesArray = localStorage.getItem('hashes').split(',');
                this.hashesArray.push(this.hashText);
                localStorage.setItem('hashes', this.hashesArray.toString());
                localStorage.setItem(this.hashText, [this.nameText, this.typeText].toString());

            } else {
                this.hashesArray = [this.hashText];
                localStorage.setItem('hashes', this.hashesArray.toString());
                localStorage.setItem(this.hashText, [this.nameText, this.typeText].toString());
            }
            this.hashText = '';
            this.nameText = '';
            this.typeText = '';
        }
    }

    Show() {
        if (typeof (Storage) !== 'undefined') {
            if (localStorage.getItem('hashes')) {
                var table: HTMLTableElement = <HTMLTableElement>document.getElementById('hashesTable');
                this.hashesArray = localStorage.getItem('hashes').split(',');
                var len = this.hashesArray.length;
                for (var i = 0; i < len; i++) {
                    var nameAndType: String[] = localStorage.getItem(this.hashesArray[i].toString()).split(',');
                    if (table.rows[i + 1]) {
                        table.deleteRow(i + 1);
                    }
                    var newRow = table.insertRow(i + 1);
                    var hashCell = newRow.insertCell(0);
                    var nameCell = newRow.insertCell(1);
                    var typeCell = newRow.insertCell(2);
                    hashCell.innerHTML = this.hashesArray[i].toString();
                    nameCell.innerHTML = nameAndType[0].toString();
                    typeCell.innerHTML = nameAndType[1].toString();
                }

            }
        }
    }
}
